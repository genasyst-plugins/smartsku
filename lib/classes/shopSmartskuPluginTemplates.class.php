<?php
class shopSmartskuPluginTemplates {
    protected static $theme_templates = array(
        'options' => 'plugin.smartsku.options.html',
        'js' => 'plugin.smartsku.js',
        'css' => 'plugin.smartsku.css'
    );
    protected static $plugin_templates = array(
        'options' => 'product.options.html',
        'js' => 'custom.js',
        'css' => 'custom.css'
    );
    protected static $templates_access = array(
        'options' => false,
        'js' => true,
        'css' => true
    );
    protected static $templates = array();
    protected static $theme = null;
    protected $settings = null;
    public function __construct($settings = null)
    {
        $this->settings = $settings;
    }
    public function getTemplate($name) {
        if(!isset(self::$templates[$name])) {
            self::$templates[$name] = '';
            if(!self::$templates_access[$name]) {
                $template_theme = self::getThemeTemplatePath(self::getTemplateFileName($name, true));
                $template_plugin = self::getPluginTemplatePath('templates/'.self::getTemplateFileName($name));
                if($this->settings['template_type'] == 'theme') {
                    if(!$template_theme) {
                        $template_theme = $template_plugin;
                    }
                    if ($template_theme) {
                        self::$templates[$name] = 'file:'.$template_theme;
                    }
                } elseif ($this->settings['template_type'] == 'custom') {
                    self::$templates[$name] =  'string: '.(string)$this->settings['template_'.$name];
                } else {
                    if ($template_plugin) {
                        self::$templates[$name] = 'file:'.$template_plugin;
                    }
                }
                if(!isset( self::$templates[$name])) {
                    self::$templates[$name] = 'string: ';
                }
            } else {
                if($this->settings['template_type'] == 'theme') {
                    $template_theme = self::getThemeTemplatePath(self::getTemplateFileName($name, true));
                    if ($template_theme) {
                        $theme = self::getTheme();
                       if($name == 'js') {
                         
                           self::$templates[$name] =  '<script type="text/javascript" src="'. $theme->getUrl().''.self::getTemplateFileName($name, true).'"></script>';
                       } elseif($name == 'css') {
                           self::$templates[$name] =  '<link href="'. $theme->getUrl().''.self::getTemplateFileName($name, true).'" type="text/css" rel="stylesheet">';
                       }
                    }
                } elseif ($this->settings['template_type'] == 'custom' && isset($this->settings['template_'.$name])) {
                    $content = trim((string)$this->settings['template_'.$name]);
                    if(!empty($content)) {
                        if($name == 'js') {
                            self::$templates[$name] =  '<script type="text/javascript"> '.(string)$this->settings['template_'.$name].' </script>';
                        } elseif($name == 'css') {
                            self::$templates[$name] =  '<style type="text/css">'.(string)$this->settings['template_'.$name].'</style>';
                        } 
                    }
                } else {
                    self::$templates[$name] = '';
                }
            }

        }
        return  self::$templates[$name];
    }
    public function getThemeCorrection() {
        $theme = self::getTheme();
        $html = '';
        $plugin_path =  realpath(dirname(__FILE__) .'/../../').DIRECTORY_SEPARATOR;
        $js_path = 'js/themes/'.$theme['id'].'.js';
        $css_path = 'css/themes/'.$theme['id'].'.css';
        if(file_exists($plugin_path.$js_path)) {
            $html .=  '<script type="text/javascript" src="'.shopSmartskuPlugin::getUrlStatic().$js_path.'"></script>';
        }
        if(file_exists($plugin_path.$css_path)) {
            $html .= '<link href="'.shopSmartskuPlugin::getUrlStatic().$css_path.'" rel="stylesheet" type="text/css">';
        }
        return $html;

    }
    public function themesTemplatesExists() {
        $themes = $this->settings->getStorefront()->getThemes();
        if(!empty($themes)) {
            foreach ($themes as $type => $theme) {
                if ($theme) {
                    foreach ($this->getThemeTemplates() as $t_key => $filename){
                        if (!self::getThemeTemplatePath($filename,$theme)) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }
    public static function getTemplatePath($name) {
        $template = self::getThemeTemplatePath(self::getTemplateFileName($name, true));
        if ($template) {
           return $template;
        } else {
            $template = self::getPluginTemplatePath('templates/'.self::getTemplateFileName($name));
            if(file_exists($template)) {
                return $template;
            } else {
               return false;
            }
        }
    }
    public static function getPluginTemplatePath($path = '') {
        $path = ltrim($path,'/\\');
        return  realpath(dirname(__FILE__) .'/../../templates/').DIRECTORY_SEPARATOR.$path;
    }
    public static function getThemeTemplatePath($path = '', $theme = null) {
        if(!is_object($theme) || !($theme instanceof waTheme)) {
            $theme = self::getTheme();
        }
        $theme_file = $theme->getPath().DIRECTORY_SEPARATOR.$path;
        if (file_exists($theme_file) /*&& self::getTheme()->getFile($path)*/) {
            return $theme_file;
        }
        return false;
    }
    public static function getTheme()
    {
        if (self::$theme == null) {
            self::$theme = new waTheme(waRequest::getTheme());
        }
        return self::$theme;
    }
    
    public static function getTemplateFileName($name = '', $is_theme = false) {
        $templates = $is_theme? self::$theme_templates : self::$plugin_templates;
        if(isset($templates[$name])) {
            return $templates[$name];
        }
        return false;
    }
    
    public static function getThemeTemplates() {
        return self::$theme_templates;
    }
    public static function getTemplatePluginContent($name) {
        $template_plugin = self::getPluginTemplatePath('templates/'.self::getTemplateFileName($name));
        if ($template_plugin) {
            return file_get_contents($template_plugin);
        }
        return '';
    }
}