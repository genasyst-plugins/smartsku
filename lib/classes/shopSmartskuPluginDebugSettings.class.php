<?php
class shopSmartskuPluginDebugSettings extends shopSmartskuPluginSettings {

    protected $storefront = null;

    public function __get($name) {
        if($name=='data') {
            return array_merge($this->data, $this->settings_objects);
        }
        return $this->$name;
    }
    protected function init() {
        $real_storefront = $this->storefront->getName();
        $key = shopSmartskuPlugin::PLUGIN_ID;
        if(empty($this->data)) {
            $this->setStorefront(shopSmartskuPlugin::GENERAL_STOREFRONT);
            $this->setSettings();
        }
        $session = wa()->getStorage();
        $session_data = wa()->getStorage()->get($key);
        if(!empty($session_data[$real_storefront])) {
            $this->data = $session_data[$real_storefront];
        } else {
            if(wa()->getEnv() == 'frontend') {
                $session_data[$real_storefront] = $this->data;
                $session->set($key, $session_data);
            }
        }

        $this->data['template_type'] = 'custom';
        $this->setSettingsObjects();
        $this->setStorefront($real_storefront);
    }

    protected function getSettingsObject($name) {
        if(array_key_exists($name, $this->settings_objects)) {
            if($this->settings_objects[$name] == null) {
                $this->setSettingsObjects();
            }
            return $this->settings_objects[$name];
        }
        return null;
    }
    /* Настройки характеристик на витрине  */
    public function getFeaturesSettings() {
        return $this->getSettingsObject('feature_settings');
    }
    /* Настройки по типу продукта  */
    public function getProductTypeSettings() {
        return $this->getSettingsObject('product_type_settings');
    }
    public function getSmartSkuSettings() {
        return $this->getSettingsObject('smart_sku_settings');
    }
    public function save($data) {
        if(is_array($data)) {
            $session = wa()->getStorage();
            $key = shopSmartskuPlugin::PLUGIN_ID;
            if(isset($data['settings'])) {
                $session_data = wa()->getStorage()->get($key);
                unset($data['settings']['template_options']); // Удаляем шаблон со смарти, а то хакнут
                $session_data[$this->getStorefront()->getName()] = array_merge($this->data, $data['settings']);
                $session->set($key, $session_data);
            }
        }
    }
}