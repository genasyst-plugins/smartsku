<?php

class shopSmartskuPluginFrontendDebugSaveController extends waJsonController {
    
    public function execute()  {
        if(shopSmartskuPlugin::isAction() && shopSmartskuPlugin::isDebugRights()) {
            $settings = shopSmartskuPlugin::getPluginSettings();
            $data = waRequest::post();
            $settings->save($data);
        }
    }
}
