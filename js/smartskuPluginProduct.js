
function var_dump() {
    var output = '', pad_char = ' ', pad_val = 4, lgth = 0, i = 0, d = this.window.document;
    var getFuncName = function (fn) {
        var name = (/\W*function\s+([\w\$]+)\s*\(/).exec(fn);
        if (!name) {
            return '(Anonymous)'
        }
        return name[1]
    };
    var repeat_char = function (len, pad_char) {
        var str = '';
        for (var i = 0; i < len; i++) {
            str += pad_char
        }
        return str
    };
    var getScalarVal = function (val) {
        var ret = '';
        if (val === null) {
            ret = 'NULL'
        } else if (typeof val === 'boolean') {
            ret = 'bool(' + val + ')'
        } else if (typeof val === 'string') {
            ret = 'string(' + val.length + ') "' + val + '"'
        } else if (typeof val === 'number') {
            if (parseFloat(val) == parseInt(val, 10)) {
                ret = 'int(' + val + ')'
            } else {
                ret = 'float(' + val + ')'
            }
        } else if (val === undefined) {
            ret = 'UNDEFINED';
        } else if (typeof val === 'function') {
            ret = 'FUNCTION';
            ret = val.toString().split("\n");
            var txt = '';
            for (var j in ret) {
                if(ret.hasOwnProperty(j)) {
                    txt += (j != 0 ? '' : '') + ret[j] + "\n"
                }
            }
            ret = txt
        } else if (val instanceof Date) {
            val = val.toString();
            ret = 'string(' + val.length + ') "' + val + '"'
        } else if (val.nodeName) {
            ret = 'HTMLElement("' + val.nodeName.toLowerCase() + '")'
        }
        return ret
    };
    var formatArray = function (obj, cur_depth, pad_val, pad_char) {
        var someProp = '';
        if (cur_depth > 0) {
            cur_depth++
        }
        var  base_pad = repeat_char(pad_val * (cur_depth - 1), pad_char);
        var thick_pad = repeat_char(pad_val * (cur_depth + 1), pad_char);
        var str = '';
        var val = '';
        if (typeof obj === 'object' && obj !== null) {
            if (obj.constructor && getFuncName(obj.constructor) === 'PHPJS_Resource') {
                return obj.var_dump();
            }
            lgth = 0;
            for (someProp in obj) {
                lgth++
            }
            str += "array(" + lgth + ") {\n";
            for (var key in obj) {
                if(obj.hasOwnProperty(key)) {
                    if (typeof obj[key] === 'object' && obj[key] !== null && !(obj[key] instanceof Date) && !obj[key].nodeName) {
                        str += thick_pad + "[" + key + "] =>\n" + thick_pad + formatArray(obj[key], cur_depth + 1, pad_val, pad_char)
                    } else {
                        val = getScalarVal(obj[key]);
                        str += thick_pad + "[" + key + "] =>\n" + thick_pad + val + "\n"
                    }
                }
            }
            str += base_pad + "}\n"
        } else {
            str = getScalarVal(obj)
        }
        return str
    };
    output = formatArray(arguments[0], 0, pad_val, pad_char);
    for (i = 1; i < arguments.length; i++) {
        output += '\n' + formatArray(arguments[i], 0, pad_val, pad_char)
    }
    return output
}
function av(data) {
    alert(var_dump(data))
}

if(typeof $.smartskuPluginProductElements != 'object') {
    $.smartskuPluginProductElements = {};
}

$.smartskuPluginProductElements.exists = function (name) {
    if(this.hasOwnProperty(name)) {
        return true;
    }
    return false;
};

$.smartskuPluginProductElements.set = function (name, data, force) {
    if(force || !this.exists(name)) {
        this[name] = data;
    }
};

$.smartskuPluginProductElements.get = function (name) {
    if(this.exists(name)) {
        return this[name];
    }
    return null;
};
///Основные селекторы
$.smartskuPluginProductElements.set('_Selectors', {
    smartsku_product_root: '.smartsku_plugin-product',
    input_product_id: '[name="product_id"]',
    input_product_id_name: 'product_id',
    div_offers: '.offers',
    price: '.price',
    compare_price:'.compare-at-price',
    compare_price_html: '<span class="compare-at-price nowrap"></span>',
    image: '.image',
    skus: '.skus',
    skus_button: '.skus input[type=radio]',     
    skus_button_container: 'label',
    services: '.services',
    services_variants: '.service-variants',
    stocks:'.stocks',
    sku_feature: '.sku-feature',
    sku_feature_value_class_active : 'selected',
    sku_feature_value_class_grey : 'smartsku_plugin-feature_value_grey',
    sku_feature_value_class_hide :'smartsku_plugin-feature_value_hide',
    sku_feature_element_data_id : 'feature-id',
    sku_feature_container: '.inline-select',
    sku_feature_button: 'a',
    sku_options: '.options',
    cart_button_plugin: '.smartsku_plugin-cart-button',
    cart_button_class: '.addtocart',
    cart_button: '[type=submit]',
    quantity: '.qty',
    added2cart: '.added2cart',
    cart: '#cart',
    form_action_indicator: 'data-url'
});
$.smartskuPluginProductElements.set('Root', function($id_element) {
    return $id_element.closest('form');
});
// Форма
$.smartskuPluginProductElements.set('Form', function(root_element) {
    return root_element;
});
// Элементы артикулов товара
$.smartskuPluginProductElements.set('Skus',   function(root_element) {
    return root_element.find(this._Selectors.skus);
});
$.smartskuPluginProductElements.set('Options',   function(root_element) {
    return root_element.find(this._Selectors.sku_options);
});
// Элементы сервисов продукта
$.smartskuPluginProductElements.set('Services',  function(root_element) {
    return root_element.find(this._Selectors.services);
});
// Элемент характеристики товара
$.smartskuPluginProductElements.set('SkuFeature',  function(root_element, id) {
    return root_element.find('[name="features['+id+']"]');
});
// Все элементы характеристик товара
$.smartskuPluginProductElements.set('SkuFeatures',  function(root_element) {
    return root_element.find(this._Selectors.sku_feature);
});
$.smartskuPluginProductElements.set('Stocks',  function(root_element) {
    return root_element.find(this._Selectors.stocks);
});
$.smartskuPluginProductElements.set('Selectors', function() {
    return this._Selectors;
});
$.smartskuPluginProductElements.set('SkuContainer',  function(sku_input) {
    return sku_input.closest(this._Selectors.skus_button_container);
});
// Управляющий класс
smartskuPluginProductElements = function($id_element) {
    this.root_element = $.smartskuPluginProductElements.Root($id_element);
};
smartskuPluginProductElements.prototype = {
    'Selectors':     function () {return $.smartskuPluginProductElements.get('_Selectors');},
    'Form':          function () {return $.smartskuPluginProductElements.Form(this.root_element);},
    'Skus':          function () {return $.smartskuPluginProductElements.Skus(this.root_element);},
    'Options':       function () {return $.smartskuPluginProductElements.Options(this.root_element);},
    'SkuFeature':    function (id) {return $.smartskuPluginProductElements.SkuFeature(this.root_element, id);},
    'SkuFeatures':   function () {return $.smartskuPluginProductElements.SkuFeatures(this.root_element);},
    'Stocks':        function () {return $.smartskuPluginProductElements.Stocks(this.root_element);},
    'Services':      function () {return $.smartskuPluginProductElements.Services(this.root_element);},
    'SkuContainer':  function (sku_input) {return $.smartskuPluginProductElements.SkuContainer(sku_input);}
};
smartskuPluginProductSkuFeature = function($feature_obj, options) {
    if(typeof options =='object') {
        for(var k in options) {
            this[k] = options[k];
        }
    }
    this.feature = $feature_obj;
    if(this.getTag()=='INPUT' && this.getElement().attr('type')=='radio') {
        this.radio = this.getElement().closest(this.Selectors().sku_feature_container).find('input[type=radio]');
        var checked = this.getElement().closest(this.Selectors().sku_feature_container).find('input[type=radio]:checked');
        if(checked.length>0) {
            this.feature = checked;

        } else {
            this.feature = this.getElement().closest(this.Selectors().sku_feature_container).find('input[type=radio]:first');
            this.setValue(this.feature.val());
        }
    }
};
smartskuPluginProductSkuFeature.prototype = {
    Selectors: function(){
        return  $.smartskuPluginProductElements.Selectors();
    },
    getId: function(){
        return this.feature.data(this.Selectors().sku_feature_element_data_id);
    },
    getElement: function () {
        return this.feature;
    },
    getButtons: function () {
        return this.feature
            .closest(this.Selectors().sku_feature_container)
            .find(this.Selectors().sku_feature_button);
    },
    getButtonValue: function($button_obj) {
        return $button_obj.data('value');
    },
    show: function () {
        var feature_block =  this.feature.closest('.smartsku-feature-block');
        feature_block.find('.smartsku-feature-value').hide();
        feature_block.find('.smartsku-feature-select').show();
    },
    hide: function() {
        var feature_block =  this.feature.closest('.smartsku-feature-block');
        feature_block.find('.smartsku-feature-value').hide();
        feature_block.find('.smartsku-feature-select').hide();
        feature_block.find('.smartsku-feature-value-'+this.getValue()).show();
    },
    getValues: function (active, sort) {
        var self = this;
        sort = sort||false;
        var tag = self.getTag();
        var values = {};
        var index = 0;
        if(tag == 'INPUT') {
            self.getButtons().each(function () {
                var val_id = self.getButtonValue($(this));
                if(!active || (!$(this).hasClass(self.getHideClass(1)) && !$(this).hasClass(self.getHideClass(2)))) {
                    if(!sort) {
                        values[val_id] = $(this).html();
                    }  else {
                        values[index] = {'id': val_id, 'value': $(this).html()};
                        index++;
                    }
                }
            });
        }  else if(tag == 'SELECT') {
            self.feature.find('option').each(function () {
                var val_id = $(this).val();
                if(!active || (!$(this).hasClass(self.getHideClass(1)) && !$(this).hasClass(self.getHideClass(2)))) {
                    if(!sort) {
                        values[val_id] = $(this).html();
                    }  else {
                        values[index] = {'id': val_id, 'value': $(this).html()};
                        index++;
                    }
                }
            });
        }
        return values;
    },
    getSimilarValues:function(value_id) {
        value_id = value_id || this.getValue();
        var f_values = this.getValues(false, true);
        var feature_values = [];
        var c = 0;
        var val_index = 0;
        // Делаем массив значений, по нему будем менять
        for(var k in f_values) {
            feature_values[c] = f_values[k].id;
            if(value_id == f_values[k].id) {
                val_index = c;
            }
            c++;
        }
        var similar_values = {};
        var ind = 1;
        for (var i = 1;i<=this.count(feature_values);i++) {
            if(feature_values.hasOwnProperty(val_index+i)){
                var val_id = feature_values[val_index+i];
                similar_values[ind] = val_id;
                ind++;
            }
            if(feature_values.hasOwnProperty(val_index-i)){
                var val_id = feature_values[val_index-i];
                similar_values[ind] = val_id;
                ind++;
            }
        }
        return similar_values;
    },
    getValue: function () {
        if(this.getTag()=='INPUT' && this.getElement().attr('type')=='radio') {
            return this.getElement()
                .closest(this.Selectors().sku_feature_container)
                .find('input[type=radio]:checked').val();
        } else {
            return this.getElement().val();
        }
    },
    getTag: function(){
        if(this.getElement().length > 0 ) {
            var tag = null;
            this.getElement().each(function () {
                if($(this).get(0).hasOwnProperty(tagName)) {
                    tag = $(this).get(0).tagName;
                    return;
                } else {
                    console.log('smartsku: no tag in element ');
                    console.log($(this));
                }
            });
            return tag;
        } else {
            if($(this).get(0).hasOwnProperty(tagName)) {
                return this.getElement().get(0).tagName;
            } else {
                console.log('smartsku: no tag in element ');
                 console.log($(this));
            }
        }
    },
    setValue: function (value_id) {
        var self = this;
        var tag = self.getTag();
        if(tag == 'INPUT') {
            if(self.getElement().attr('type')=='hidden') {
                self.setHiddenValue(value_id);
            } else if(self.getElement().attr('type')=='radio') {
                self.setRadioValue(value_id);
            } else {
                console.log('SmartskuPlugin: Неизвестный тип характеристики-'+self.getElement().attr('type')+";\n");
            }
        }  else if(tag == 'SELECT') {
            self.setSelectValue(value_id);
        } else {
            console.log('SmartskuPlugin: Неизвестный тип характеристики-'+self.getElement().attr('type')+";\n");
        }
    },
    setHiddenValue: function(value_id){
        var self = this;
        self.getElement().val(value_id);
        self.setButtonsValue(value_id);
    },
    setRadioValue: function(value_id) {
        var self = this;
        self.getElement()
            .closest(this.Selectors().sku_feature_container).find('input[type=radio]').prop('checked', false).removeAttr('checked', false);
        //self.getElement().find('[value="' + value_id + '"]').attr('checked','checked').prop('checked', true);
        this.feature = self.getElement()
            .closest(this.Selectors().sku_feature_container)
            .find('input[value="' + value_id + '"]');
        this.feature.attr('checked', 'checked').prop('checked', true);
        //self.getElement().val(value_id);
        self.setButtonsValue(value_id);
    },
    setSelectValue: function(value_id) {
        var self = this;
        self.getElement().val(value_id);
        self.getElement().find('option[value="' + value_id + '"]')
            .removeClass(self.getHideClass(1)).removeClass(self.getHideClass(2));
        if(self.getButtons().length>0) {
            self.setButtonsValue(value_id);
        }
    },
    setButtonsValue: function(value_id) {
        var self = this;
        self.getButtons().each(function ()  {
            var val_id = self.getButtonValue($(this));
            var button = $(this);
            if(value_id == val_id) {
                self.setButtonState(button, 1);
            } else {
                self.setButtonState(button, 0);
            }
        });
    },
    /* state int 0-1-2 */
    setButtonState: function(button, state) {
        var self = this;
        if(state===1) {
            button
                .addClass(this.Selectors().sku_feature_value_class_active)
                .removeClass(self.getHideClass(1))
                .removeClass(self.getHideClass(2));
        } else if(state===0) {
            button.removeClass(this.Selectors().sku_feature_value_class_active);
        }
    },
    hideButton: function(button, type) {
        var self = this;
        setTimeout(
            function(){
                button
                    .removeClass(self.getHideClass(1))
                    .removeClass(self.getHideClass(2))
                    .addClass(self.getHideClass(type));
            }
            ,10);
    },
    showValue: function (value_id) {
        this.hideValue(value_id, 0);
    },
    hideValue: function(value_id, type) {
        var self = this;
        var tag = self.getTag();
        if(tag == 'INPUT') {
            // возможно кто-то сделает на чистых Radio, тогда добавлю
        } else if(tag == 'SELECT') {
            self.feature.find('option[value="'+value_id+'"]')
                .removeClass(self.getHideClass(1))
                .removeClass(self.getHideClass(2))
                .addClass(self.getHideClass(type));
        }
        self.getButtons().each(function () {
            var val_id = self.getButtonValue($(this));
            if(value_id == val_id) {
                self.hideButton($(this), type);
                return;
            }
        });
    },
    getHideClass: function (type) {
        if(type == 1) {
            return this.Selectors().sku_feature_value_class_grey;
        } else if(type == 2) {
            return this.Selectors().sku_feature_value_class_hide;
        }
        return '';
    },
    count:function (mixed_var, mode) {
        var key, cnt = 0;
        if (mode == 'COUNT_RECURSIVE')mode = 1;
        if (mode != 1)mode = 0;
        for (key in mixed_var) {
            cnt++;
            if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
                cnt += this.count(mixed_var[key], 1)
            }
        }
        return cnt;
    }
};
// Класс поиска артикулов, умное переключение артикулов
function smartskuPluginFindSku (product_elements, sku_features, options) {
    this._elements = product_elements;
    this.form =  product_elements.Form();
    this.sku_features = sku_features;
    this.features = {};
    this.debug = 0;
    this.sku_variants = {};

    this.smart_sku = 0;
    this.smart_sku_replace = 0;
    this.smart_sku_hide_non_existent_type = 0;
    this.smart_sku_hide_not_available_type = 0;
    this.smart_sku_hide_single_feature = 0;
    this.smart_sku_hide_multi_feature = 0;

    this.smart_sku_class_grey = '';
    this.smart_sku_class_hide = '';
    this.debug_message = '';
    this.features_values_variants = false;
    /*
     *  'smart_sku'                         => 1, // Общая настройка
     'smart_sku_replace'                 => 1, // Менять ли артикул на доступный
     'smart_sku_hide_single_feature'     => 1, // Скрывать характеристику если всего один вариант выбора
     'smart_sku_hide_not_available_type' => 1, // Тип скрытия характеристик недоступного артикула
     'smart_sku_hide_non_existent_type'  => 1, // Тип скрытия характеристик несуществующего артикула
     'smart_sku_hide_style'              => 0,  // Свои классы для  скрытия
     'smart_sku_class_grey'              => '', // Класс частичного скрытия
     'smart_sku_class_hide'	            => '', // Класс полного скрытия
     */
    // Ставим опции
    for(var k in options) {
        if(options.hasOwnProperty(k)) {
            this[k] = options[k];
        }
    }
    this.algorithm = 1;
    $.smartskuPluginProductElements.Selectors().sku_feature_value_class_grey = this.getHideClass(1);
    $.smartskuPluginProductElements.Selectors().sku_feature_value_class_hide  = this.getHideClass(2);
    // Сетим характеристики
    var self = this;
    this.getElements().SkuFeatures().each(function () {
        var feature = new smartskuPluginProductSkuFeature($(this));
        self.features[self.getFeatureSid(feature.getId())] = feature;
    });

}
smartskuPluginFindSku.prototype = {
    'getElements': function () {
        return this._elements;
    },
    'getSku' : function (feature_obj) {
        var self = this;
        var sku = false;
        var sku_key = self.getSkuByValues();
        if(sku_key) {
            sku = self.sku_features[sku_key];
            if(self.debug=='1') {
                self.debug_message ='Smartsku_Plugin:findSku:getSku '+"\n"+'Артикул Найден - '+sku_key+";\n";
                self.debug_message +=var_dump(sku)+"\n";
                self.debug_message +=var_dump(self.sku_features)+"\n";
                if(!sku.available && self.smart_sku_replace=='1'){
                    self.debug_message +='Артикул не доступен для покупки;'+"\n";
                }
            }
        }  else {
            if(self.debug=='1') {
                self.debug_message ='Smartsku_Plugin:findSku:getSku '+"\n"+'Артикул не существует;'+";\n";
            }
        }
        if ((!sku || !sku.available) && self.smart_sku_replace=='1') {
            sku = self.findSku(feature_obj);
        } else {
            self.hideSkuFeaturesValues(feature_obj);
        }
        if(self.debug=='1') {
            console.log(self.debug_message);
        }
        return sku;
    },
    'getFeatureSid' : function (id) {
        return "f"+id;
    },
    'getFeatureId' : function (id) {
        return id.replace('f','');
    },
    'findSku': function (changed_feature_obj) {
        var self = this;
        if(self.debug=='1') {
            self.debug_message +='Запущен поиск артикула...;'+"\n";
        }
        changed_feature_obj = new smartskuPluginProductSkuFeature(changed_feature_obj);
        var value_id = changed_feature_obj.getValue();
        var changed_feature_id = changed_feature_obj.getId();
        var features_values_variants = this.getFeaturesValuesVariants(true);
        var features = features_values_variants[value_id];
        var sku_key = false;
        if(self.count(features)>0) {
            var variants = {};
            for (var k in features) {
                for (var fk in features[k]) {
                    for (var sku_d in features[k][fk]) {
                        if (typeof(variants[sku_d]) != 'object') {
                            variants[sku_d] = {}
                        }
                        variants[sku_d][k] = fk;
                    }
                }
            }
            var max_similar = {
                'count_features':0,
                'features_identical':0,
                'similar_offset': 100,
                'feature_available':0
            };
            var count_sku_features = {};
            for (var vk in variants) {
                var variant = variants[vk];
                var features_identical = 0;
                var similar_offset = 0;
                var available = 0;
                var sku = self.sku_features[vk];
                for(var id in self.features) {
                    var feature_obj = self.features[id];
                    var feature_id = feature_obj.getId();
                    var feature_value = feature_obj.getValue();
                    if (typeof(variant[feature_id]) !== 'undefined') {
                        if (variant[feature_id] == feature_value) {
                            features_identical++;
                        } else {
                            var similar_values = feature_obj.getSimilarValues(feature_obj.getValue());
                            for(var offset in similar_values) {
                                if(similar_values[offset]==variant[feature_id]) {
                                    similar_offset += parseInt(offset);
                                }
                            }
                        }
                    }
                }
                if(variant[changed_feature_obj.getId()] == value_id && (self.smart_sku_replace=='0' || sku.available)) {
                    available = 1;
                }
                var count_features = this.count(variant);
                count_sku_features[vk] = {};
                count_sku_features[vk]['available'] = available;
                count_sku_features[vk]['similar_offset'] = similar_offset;
                count_sku_features[vk]['count_features'] = count_features;
                count_sku_features[vk]['features_identical'] = features_identical;
                if(self.debug=='1') {
                    count_sku_features[vk]['sku_available'] = sku.available;
                    count_sku_features[vk]['feature_id'] = changed_feature_id;
                    count_sku_features[vk]['value_id'] = variant[changed_feature_id];
                }
                if ((self.smart_sku_replace=='0' || sku.available) &&
                    similar_offset     <= max_similar['similar_offset'] &&
                    available          >= max_similar['feature_available'] &&
                    count_features     >= max_similar['count_features'] &&
                    features_identical >= max_similar['features_identical'] ) {
                    max_similar['count_features'] = count_features;
                    max_similar['features_identical'] = features_identical;
                    max_similar['similar_offset'] = similar_offset;
                    max_similar['feature_available'] = available;
                }
            }
            for (var ck in count_sku_features) {
                if (count_sku_features[ck]['available'] === max_similar['feature_available']
                    && count_sku_features[ck]['similar_offset'] <= max_similar['similar_offset']
                    && count_sku_features[ck]['count_features'] === max_similar['count_features']
                    && count_sku_features[ck]['features_identical'] === max_similar['features_identical']
                ) {
                    sku_key = ck;
                    break;
                }
            }
            if(self.debug=='1') {
                self.debug_message += 'Smartsku:findsku '+"\n"+
                    'Варианты '+var_dump(count_sku_features)+"\n"+
                    'Максимальная схожесть ' +var_dump(max_similar)+"\n"+
                    'Артикул '+sku_key+"\n\n\n";
            }
        } else {
            sku_key = self.getSkuByValues();
        }
        if(!sku_key) {
            var similar_values = changed_feature_obj.getSimilarValues();
            for(var k in similar_values) {
                changed_feature_obj.setValue(similar_values[k]);
                if(changed_feature_obj.getValue()!=similar_values[k]) {
                    self.debug_message +='Smartsku_plugin:findsku Не установлено новое значение характеристики: '+similar_values[k]+'!;'+"\n";
                }
                var sku_key_temp = self.getSkuByValues();
                if(sku_key_temp) {
                    sku_key = sku_key_temp;
                    break;
                }
            }
        }
        if (sku_key) {
            if(self.debug==1) {
                self.debug_message +='Smartsku_plugin:findsku Артикул найден: '+sku_key+'!;'+"\n";
            }
            var sku_features = self.getSkuKeyFeaturesValues(sku_key);
            for(var id in sku_features) {
                if(self.features.hasOwnProperty(self.getFeatureSid(id))) {
                    self.features[self.getFeatureSid(id)].setValue(sku_features[id]);
                    if(self.features[self.getFeatureSid(id)].getValue() != sku_features[id]) {
                        self.debug_message +='Smartsku_plugin:findsku Не установлено новое значение характеристики: '+sku_features[id]+'!;'+"\n";
                    }
                }
            }
            sku = self.sku_features[sku_key];
            self.getElements().SkuFeatures().change();
        } else {
            self.debug_message +='Smartsku_plugin: Ни одного доступного артикула не найдено!;'+"\n";
        }
        self.hideSkuFeaturesValues(changed_feature_obj);
        return sku;
    },
    'getSkuByValues': function() {
        var self = this;
        var key_arr = [];
        var sku_key = false;
        var ki = 0;
        for(var id in self.features) {
            key_arr[ki++] = self.getFeatureId(id) + ':' + self.features[id].getValue() + ';'
        }
        var variants = self.getSkuVariants(key_arr);
        for(var s_key in self.sku_features) {
            var sku = self.sku_features[s_key];
            if(variants.hasOwnProperty(s_key)) {
                if(sku.available || (self.smart_sku_replace=='0')) {
                    sku_key = s_key;
                    break;
                }
            }
        }
        return sku_key;
    },
    'getAlgorithm' :function () {
        return this.algorithm;
    },
    'getSkuVariants' :function (arr) {
        this.sku_variants = {};
        if(this.getAlgorithm() == 2) {
            this.generateSkuVariants(arr);
        } else {
            var def_sku = arr.join('');
            this.sku_variants[def_sku] = def_sku;
        }
        return this.sku_variants
    },
    'generateSkuVariants':function (arr, temp, level) {
        if (typeof(level) != 'number') {
            level = 0
        }
        for (var k in arr) {
            if (level < 1) {
                temp = ''
            }
            var arr1 = arr.slice();
            delete arr1[k];
            if (this.count(arr1) > 0) {
                this.generateSkuVariants(arr1, temp + '' + arr[k], level + 1)
            } else {
                this.sku_variants[temp + '' + arr[k]] = temp + '' + arr[k]
            }
        }
    },
    'getHideClass' : function (type) {
        if(type==1) {
            return this.smart_sku_class_grey;
        }else if(type==2) {
            return this.smart_sku_class_hide;
        }
        return 'smartsku_plugin-feature_hide';
    },
    'hideSkuFeaturesValues': function (changed_feature_obj) {
        var self = this;
        var features_values_variants = this.getFeaturesValuesVariants();
        if(this.smart_sku_hide_non_existent_type == '0' && this.smart_sku_hide_not_available_type =='0') {
            return;
        }
        if(self.count(self.features)==1) {
            // Скрываем значения несуществующих артикулов
            var count_available_sku = 0;
            for(var id in self.features) {
                var f_values = self.features[id].getValues();
                for(var k in f_values) {
                    var sku = false;
                    if(self.sku_features.hasOwnProperty(self.getFeatureId(id)+':'+ k+';')){
                        sku = self.sku_features[self.getFeatureId(id)+':'+ k+';'];
                    }
                    if(!sku && self.smart_sku_hide_non_existent_type !='0') {
                        self.features[id].hideValue(k,  self.smart_sku_hide_non_existent_type);
                    } else if(sku && self.smart_sku_hide_not_available_type !='0' && !sku.available) {
                        self.features[id].hideValue(k, self.smart_sku_hide_not_available_type);
                    } else {
                        count_available_sku++;
                    }
                }
            }
            // Скрытие характеристики, если доступен только 1 вариант и нельзя выбрать другие
            if(count_available_sku < 2 && parseInt(self.smart_sku_hide_single_feature) == 1 && parseInt(self.smart_sku_replace)==1 && self.smart_sku_hide_not_available_type !='0') {
                for(var id in self.features) {
                    self.features[id].hide();
                }
            } else {
                for(var id in self.features) {
                    self.features[id].show();
                }
            }
        }
        else if(self.count(self.features) > 1) {
            for(var id in self.features) {
                var value_id = self.features[id].getValue();
                if (features_values_variants.hasOwnProperty(value_id)) {
                    var features = features_values_variants[value_id];
                    for (var k in features) {
                        if (k != self.getFeatureId(id)) {
                            var feature_obj = self.getSkuFeature(k);
                            var available_sku_values = 0;
                            var values = features[k];
                            var feature_values = feature_obj.getValues();
                            for(var val_id in feature_values) {
                                if (!values.hasOwnProperty(val_id) && parseInt(self.smart_sku_hide_non_existent_type) > 0) {
                                    feature_obj.hideValue(val_id, self.smart_sku_hide_non_existent_type);
                                } else {
                                    var sku_available = false;
                                    for (var sku_key in values[val_id]) {
                                        if (values[val_id][sku_key]==true) {
                                            sku_available = true;
                                        }
                                    }
                                    if (sku_available == false && parseInt(self.smart_sku_hide_not_available_type) > 0) {
                                        feature_obj.hideValue(val_id, self.smart_sku_hide_not_available_type);
                                    } else {
                                        available_sku_values++;
                                        feature_obj.showValue(val_id);
                                    }
                                }
                            }
                            if (available_sku_values < 2 && parseInt(self.smart_sku_hide_multi_feature) == 1 && parseInt(self.smart_sku_replace) == 1) {
                                feature_obj.hide();
                            } else {
                                feature_obj.show();
                            }
                        }
                    }
                } else {
                    self.features[id].hideValue(value_id, self.smart_sku_hide_not_available_type);
                }
            }
        }
    },
    'getSkuFeature': function (id){
        if(this.features.hasOwnProperty(this.getFeatureSid(id))) {
            return this.features[this.getFeatureSid(id)];
        }
    },
    'getFeaturesValuesVariants': function (include) {
        if(!this.features_values_variants) {
            this.features_values_variants = {};
            var features_values_variants = {};
            var features_values_variants_all = {};
            for (var sku_id in this.sku_features) {
                var available = false;
                if(this.sku_features.hasOwnProperty(sku_id) && this.sku_features[sku_id].available) {
                    available = true;
                }
                var features = this.getSkuKeyFeaturesValues(sku_id);
                for (var feature_id in features) {
                    var feature_value = features[feature_id];
                    if (typeof(features_values_variants[feature_value]) !== 'object') {
                        features_values_variants[feature_value] = {};
                    }
                    if (typeof(features_values_variants_all[feature_value]) !== 'object') {
                        features_values_variants_all[feature_value] = {};
                    }
                    for (var feature_val_id in features) {
                        var feature_val_value = features[feature_val_id];
                        if (feature_id != feature_val_id) {
                            if (typeof(features_values_variants[feature_value][feature_val_id]) != 'object') {
                                features_values_variants[feature_value][feature_val_id] = {};
                            }
                            if (typeof(features_values_variants[feature_value][feature_val_id][feature_val_value]) != 'object') {
                                features_values_variants[feature_value][feature_val_id][feature_val_value] = {}
                            }
                            features_values_variants[feature_value][feature_val_id][feature_val_value][sku_id] = available;
                        }
                        if (typeof(features_values_variants_all[feature_value][feature_val_id]) != 'object') {
                            features_values_variants_all[feature_value][feature_val_id] = {};
                        }
                        if (typeof(features_values_variants_all[feature_value][feature_val_id][feature_val_value]) != 'object') {
                            features_values_variants_all[feature_value][feature_val_id][feature_val_value] = {}
                        }
                        features_values_variants_all[feature_value][feature_val_id][feature_val_value][sku_id] = available;
                    }
                }
            }
            this.features_values_variants['other'] = features_values_variants;
            this.features_values_variants['all'] = features_values_variants_all;
        }
        if(include) {
            return  this.features_values_variants['all'];
        }
        return this.features_values_variants['other'];
    },
    'getSkuKeyFeaturesValues': function (sku_key) {
        var features = sku_key.split(';');
        var return_features = {};
        for (var fk in features) {
            if (features[fk] != '') {
                var feature_arr = features[fk].split(':');
                var feature_id = feature_arr[0];
                return_features[feature_id] = feature_arr[1];
            }
        }
        return return_features
    },
    'count' :function (mixed_var, mode) {
        var key, cnt = 0;
        if (mode == 'COUNT_RECURSIVE')mode = 1;
        if (mode != 1)mode = 0;
        for (key in mixed_var) {
            cnt++;
            if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
                cnt += this.count(mixed_var[key], 1)
            }
        }
        return cnt
    }
};
// Альтернативный класс Product
function smartskuPluginProduct(selector_id, options) {
    /* Класс выборки элементов продукта */
    this.__elements = new smartskuPluginProductElements($(selector_id));
    this.debug = 0;
    this.find_Sku = null;
    this.features = {};
    this.smartsku = {};
    this.skus = {};
    this.hide_stocks = 0;
    this.hide_services = 0;
    this.sku = {}; // skus[sku_id] default sku
    this.uid = 0;
    this.type_id = 0;
    this._parent = null; // элеменкт контейнера продуктов
    this.form = this.getElements().Form();
    for (var k in options) {
        this[k] = options[k];
    }
    this.init();
}
smartskuPluginProduct.prototype.binds = {
    'sku_features': function(self){
        var sel = self.getElements().Selectors();
        if(self.getForm().find(sel.sku_feature).length>0) {
            self.getForm().find(sel.sku_feature).bind('change',function () {
                var feature = new smartskuPluginProductSkuFeature($(this));
                if(feature.getTag()!='INPUT') {
                    self.setSkuByFeature($(this));
                }
            });
            setTimeout(function(){
                self.getForm().find(sel.sku_feature+":first").change();
            },100);
        }
    },
    'sku_features_inline': function(self){
        var sel = self.getElements().Selectors();
        if(self.getForm().find(sel.sku_feature_container).length>0) {
            self.getForm().find(sel.sku_feature_container).find(sel.sku_feature_button).bind('click', function () {
                var button = $(this);
                setTimeout(function(){
                    var feature_obj = button.closest(sel.sku_feature_container).find(sel.sku_feature);
                    var feature = new smartskuPluginProductSkuFeature(feature_obj);
                    feature.setValue(feature.getButtonValue(button));
                    feature.getElement().change();
                    self.setSkuByFeature(feature_obj);
                },1);
            });
            setTimeout(function(){
                var feature = new smartskuPluginProductSkuFeature(self.getForm().find(sel.sku_feature+":first"));
                if(feature.getButtons().length>0) {
                    feature.getButtons().each(function () {
                        if(feature.getButtonValue($(this))==feature.getValue()) {
                            $(this).click();
                        }
                    });
                }
            },30);
        }
    },
    'skus': function(self) {
        var sel = self.getElements().Selectors();
        var smartsku = self.smartsku;
        if(smartsku.smart_sku && self.getForm().find(sel.skus_button).length>0 && self.hasOwnProperty('skus') && self.count(self.skus)>0) {
            self.getForm().find(sel.skus_button).bind('click', function () {
                var sku_id = $(this).val();
                var sku = false;
                if(self.skus.hasOwnProperty(sku_id)) {
                    sku = self.skus[sku_id];
                    if(smartsku.smart_sku_replace=='1' && !sku.available) {
                        // Ставим доступный артикул
                        var similar_skus = self.getSimilarSkus(sku_id);
                        for (var k in similar_skus) {
                            var sku_id = similar_skus[k];
                            if(self.skus.hasOwnProperty(sku_id)) {
                                sku = self.skus[sku_id];
                                if(sku.available) {
                                    self.getForm().find(sel.skus_button).removeAttr('checked');
                                    var el = false;
                                    self.getForm().find(sel.skus_button).each(function () {
                                        if($(this).val()==sku_id) {
                                            el = $(this);
                                        }
                                    });
                                    self.getForm().find(sel.skus_button).prop('checked', false).attr('checked',false);
                                    setTimeout(function(){
                                        el.attr('checked','checked').prop('checked', true).click();
                                    },20);
                                    break;
                                }
                            }
                        }
                    }
                }
                self.skuFunctions(sku);
            });
        }
        //
        var $initial_cb =  self.getForm().find(".skus input[type=radio]:checked:not(:disabled)");
        if (!$initial_cb.length) {
            $initial_cb =  self.getForm().find(".skus input[type=radio]:not(:disabled):first").prop('checked', true).click();
        }
        $initial_cb.click();

        // Скрываем недоступные
        self.getForm().find(sel.skus_button).each(function () {
            var sku_id = $(this).val();
            if(self.skus.hasOwnProperty(sku_id)) {
                var sku = self.skus[sku_id];
                if(!sku.available) {
                        self.getElements().SkuContainer($(this)).addClass(self.getHideClass(smartsku.smart_sku_hide_not_available_type));
                }
            } else {
                self.getElements().SkuContainer($(this)).addClass(self.getHideClass(smartsku.smart_sku_hide_not_available_type));
            }
        });
    }
};

smartskuPluginProduct.prototype.getHideClass = function (type) {
    if(type==1) {
        return this.smartsku.smart_sku_class_grey;
    }else if(type==2) {
        return this.smartsku.smart_sku_class_hide;
    }
    return 'smartsku_plugin-feature_hide';
};
smartskuPluginProduct.prototype.getSimilarSkus = function(sku_id) {
    var sel = this.getElements().Selectors();
    var skus = this.getForm().find(sel.skus_button);
    var skus_ids = [];
    var c = 0;
    var sku_index = 0;
    // Делаем массив значений, по нему будем менять
    skus.each(function() {
        skus_ids[c] = $(this).val();
        if(sku_id ==  $(this).val()) {
            sku_index = c;
        }
        c++;
    });
    var similar_values = {};
    var ind = 1;
    for (var i = 1;i<=this.count(skus_ids);i++) {
        if(skus_ids.hasOwnProperty(sku_index+i)){
            var sku_id = skus_ids[sku_index+i];
            similar_values[ind] = sku_id;
            ind++;
        }
        if(skus_ids.hasOwnProperty(sku_index-i)){
            var sku_id = skus_ids[sku_index-i];
            similar_values[ind] = sku_id;
            ind++;
        }
    }
    return similar_values;
};
smartskuPluginProduct.prototype.bind = function () {
    var self = this;
    for(var k in this.binds) {
        var func = this.binds[k];
        if(typeof func === 'function') {
            func(self);
        }
    }
};
smartskuPluginProduct.prototype.init = function () {
    this.getElements().root_element.addClass(this.getElements().Selectors().smartsku_product_root.replace(/\./g, ''));
    if(this.debug =='1') {
        this._debug();
    }
    this.bind();
    if(this.hide_stocks=='1') {
        this.hideStocks();
    }
    if(this.hide_services == '1') {
        this.hideServices();
    }
};
smartskuPluginProduct.prototype.getDebugMessage = function (index, flag) {
    var self = this;
    var el = self.getElements();
    var sel = self.getElements().Selectors();
    var messages = {
        '-form_action_indicator': 'Индикатор отправки формы не найден ('+sel.form_action_indicator+')',
        '+form_action_indicator': 'Найден',
        '-skus': 'Не найден внутри тега форм контейнер характеристик ('+sel.skus+')',
        '+skus': 'Найден',
        '-skus_button': 'Не найдены внутри тега форм элементы показа артикулов ('+sel.skus_button+')',
        '+skus_button': 'Найдены',
        '-options': 'Не найден внутри тега форм контейнер характеристик ('+sel.sku_options+')',
        '+options': 'Найден',
        '-sku_feature': 'Не найдены внутри тега форм элементы показа характеристик ('+sel.sku_feature+')',
        '+sku_feature': 'Найден',
        '-sku_feature_data_id': 'Не найдены атрибуты id арактеристик ('+sel.sku_feature_element_data_id+')',
        '+sku_feature_data_id': 'Найден',
        '-sku_feature_inline_container': 'Не найдены контейнеры кнопок характеристик внутри тега форм ('+sel.sku_feature_container+')',
        '+sku_feature_inline_container': 'Найден',
        '-sku_feature_button': 'Не найдены кнопки характеристик ('+sel.sku_feature_button+')',
        '+sku_feature_button': 'Найден',
        'product': ' продукт не имеет вариаций для выбора артикулов'
    };
    var key = (flag==1) ?'+':'-';
    if(messages.hasOwnProperty(key+index)) {
        return key+'Smartsku_Plugin:'+index+' '+messages[key+index]+';'+"\n";
    }
    return 'Smartsku_Plugin:'+index+' Не известная ошибка;'+"\n";
};
smartskuPluginProduct.prototype._debug = function ()  {
    var self = this;
    var el = self.getElements();
    var sel = self.getElements().Selectors();
    var count_bags = 0;
    var message = '';
    if(self.count(self.skus)>1 || self.count(self.features)>0) {
        if(self.getForm().attr(sel.form_action_indicator)==undefined) {
            message += self.getDebugMessage('form_action_indicator', 0);
            count_bags++;
        } else {
            message += self.getDebugMessage('form_action_indicator', 1);
        }
        if(self.count(self.skus)>1) {
            if(self.getForm().find(sel.skus).length<1) {
                message += self.getDebugMessage('skus', 0);
                count_bags++;
            } else {
                message += self.getDebugMessage('skus', 1);
            }
            if(self.getForm().find(sel.skus_button).length<1){
                message += self.getDebugMessage('skus_button', 0);
                count_bags++;
            } else {
                message += self.getDebugMessage('skus_button', 1);
            }
        } else if(self.count(self.features)>0) {
            if(self.getForm().find(sel.sku_options).length<1) {
                message += self.getDebugMessage('options', 0);
                count_bags++;
            } else {
                message += self.getDebugMessage('options', 1);
            }
            if(self.getForm().find(sel.sku_feature).length<1) {
                message += self.getDebugMessage('sku_feature', 0);
                count_bags++;
            } else {
                message += self.getDebugMessage('sku_feature', 1);
                var inline_count = 0;
                var feature_data_id = true;
                self.getForm().find(sel.sku_feature).each(function () {
                    if($(this).data(sel.sku_feature_element_data_id)== undefined) {
                        feature_data_id = false;
                    }
                    if($(this).get(0).tagName=='INPUT' && $(this).attr('type')!='radio') {
                        inline_count++;
                    }
                });
                if(!feature_data_id) {
                    message += self.getDebugMessage('sku_feature_data_id', 0);
                    count_bags++;
                } else {
                    message += self.getDebugMessage('sku_feature_data_id', 1);
                }
                if(self.getForm().find(sel.sku_feature_container).length < inline_count) {
                    message += self.getDebugMessage('sku_feature_inline_container', 0);
                    count_bags++;
                } else {
                    message += self.getDebugMessage('sku_feature_inline_container', 1);
                    if(inline_count>0 && self.getForm().find(sel.sku_feature_container).find(sel.sku_feature_button).length<1){
                        message += self.getDebugMessage('sku_feature_button', 0);
                        count_bags++;
                    } else {
                        message += self.getDebugMessage('sku_feature_button', 1);
                    }
                }
            }
        }
    }
    else {
        message += self.getDebugMessage('product', 0);
    }
    message += 'Smartsku_Plugin:smart_sku_class_grey '+self.smartsku.smart_sku_class_grey+";\n";
    message += 'Smartsku_Plugin:smart_sku_class_hide '+self.smartsku.smart_sku_class_hide+";\n";
    message += var_dump(this.smartsku)+";\n";

    if(count_bags<1){
        message = 'Продукт '+self.getUid()+':Ок'+"\n"+message;
    } else {
        message = 'Продукт '+self.getUid()+': ---------------------!!!!!!!!!!!!!!!!! Багов найдено '+count_bags+' !!!!!!!!!!!!!!!!!!-----------------'+"\n"+message;
    }
    console.log(message+"\n");
};
smartskuPluginProduct.prototype.getElements = function () {
    return this.__elements;
};
smartskuPluginProduct.prototype.getForm = function () {
    return this.form;
};

smartskuPluginProduct.prototype.getType = function () {
    return this.type_id;
};
smartskuPluginProduct.prototype.getUid = function () {
    return this.uid;
};
// Поиск артикула по измененной характеристике
smartskuPluginProduct.prototype.findSku = function (feature_obj) {
    if(this.find_Sku==null) {
        this.smartsku['debug'] = this.debug;
        this.find_Sku = new smartskuPluginFindSku(this.getElements(), this.features, this.smartsku);
    }
    return this.find_Sku.getSku(feature_obj);
};

// Установка артикула по значению характеристики
smartskuPluginProduct.prototype.setSkuByFeature = function (feature_obj) {
    var self = this;
    var sku = this.findSku(feature_obj);
    this.skuFunctions(sku);
    if(sku) {
        this.setSku(sku);
        if(typeof ($.saleskuPluginProductsPool) == 'object' && $.saleskuPluginProductsPool.hasOwnProperty('getFeatureHideClass')) {
            if(!$.saleskuPluginProductsPool.current_change_product) {
                $.saleskuPluginProductsPool.changeFeature(feature_obj, self);
            }
        }
    }
};
smartskuPluginProduct.prototype.skuFunctions =  function (sku) {
    var self = this;
    if(typeof(self.skuBinds) == 'object' && self.count(self.skuBinds) > 0) {
        for(var id in self.skuBinds) {
            var func = self._bind(self.skuBinds[id],{'sku': sku});
            if (typeof func === "function") {
                func();
            }
        }
    }
};
smartskuPluginProduct.prototype._bind = function(func, context /*, args*/) {
    var bindArgs = [].slice.call(arguments, 2); // (1)
    function wrapper() {                        // (2)
        var args = [].slice.call(arguments);
        var unshiftArgs = bindArgs.concat(args);  // (3)
        return func.apply(context, unshiftArgs);  // (4)
    }
    return wrapper;
};
smartskuPluginProduct.prototype.skuBinds = {};
smartskuPluginProduct.prototype.setSku = function (sku_data) {};
smartskuPluginProduct.prototype.isRelatedSku = function(){
    return true;
};
smartskuPluginProduct.prototype.showStocks = function(){
    this.getElements().Stocks().show();
};
smartskuPluginProduct.prototype.hideStocks = function(){
    this.getElements().Stocks().hide();
};
smartskuPluginProduct.prototype.showServices = function(){
    this.getElements().Services().show();
};
smartskuPluginProduct.prototype.hideServices = function() {
    this.getElements().Services().hide();
};
smartskuPluginProduct.prototype.count = function (mixed_var, mode) {
    var key, cnt = 0;
    if (mode == 'COUNT_RECURSIVE')mode = 1;
    if (mode != 1)mode = 0;
    for (key in mixed_var) {
        cnt++;
        if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
            cnt += this.count(mixed_var[key], 1)
        }
    }
    return cnt;
};
smartskuPlugin = {
    init: function () {
        setInterval(function(){
            $('input[name="product_id"]').each(function() {
                var input = $(this);
                var form = input.closest('form');
                var indicator = true;
                if(form) {
                    var indicator = form.attr($.smartskuPluginProductElements.Selectors().form_action_indicator);
                }
                if(!indicator && !form.find('.smartsku-id').hasClass('smartsku-id') && !form.find('.salesku-id').hasClass('salesku-id')) {
                    input.after('<span class="smartsku-id"></span>');
                    $.get(shop_smartsku_wa_url+'smartsku_plugin_product/'+input.val()+'/',function (response) {
                        input.after(response);
                    });
                }
            });
        },100);
    }
};
$(document).ready(function () {
    smartskuPlugin.init();
});
