/* Тут будут надстройки для тем */
$.smartskuPluginProductElements._Selectors.sku_feature_container = '.row-fluid';
//$.smartskuPluginProductElements._Selectors.sku_feature_value_class_active = 'active';


smartskuPluginProductSkuFeature.prototype.getButtonValue = function($button_obj) {
    return $button_obj.data('feature-id');
};

smartskuPluginProductSkuFeature.prototype.getButtons =  function () {
    return this.feature
        .closest($.smartskuPluginProductElements.Selectors().sku_feature_container).prev()
        .find($.smartskuPluginProductElements.Selectors().sku_feature_button);
};

smartskuPluginProductSkuFeature.prototype.setButtonState = function(button,state) {
    var self = this;
    if(state===1) {
        button.find('li').addClass(self.Selectors().sku_feature_value_class_active);
        button
            .removeClass(self.getHideClass(1))
            .removeClass(self.getHideClass(2));
    } else if(state===0) {
        button.find('li').removeClass(self.Selectors().sku_feature_value_class_active);
    }
};
smartskuPluginProductSkuFeature.prototype.getValues = function (active, sort) {
    var self = this;
    sort = sort||false;
    var values = {};
    var index = 0;
        self.feature.find('option').each(function () {
            var val_id = $(this).val();
            if(!active || (!$(this).hasClass(self.getHideClass(1)) && !$(this).hasClass(self.getHideClass(2)))) {
                if(!sort) {
                    values[val_id] = $(this).html();
                }  else {
                    values[index] = {'id': val_id, 'value': $(this).html()};
                    index++;
                }
            }
        });
    return values;
};

smartskuPluginProduct.prototype.binds.sku_features_inline =  function(self){
    var sel = self.getElements().Selectors();
    if(self.getForm().find(sel.sku_feature_container).length>0) {
        self.getForm().find(sel.sku_feature_container).find(sel.sku_feature_button).bind('click', function () {
            var button = $(this);
            setTimeout(function(){
                if(button.closest('ul').data('feature-select-arrived')) {
                    var id = button.closest('ul').data('feature-select-arrived');
                } else if (button.closest('ul').data('id')){
                    var id = button.closest('ul').data('id');
                }
                var feature_obj = $('[name="features['+button.closest('ul').data('feature-select-arrived')+']"]');
                var feature = new smartskuPluginProductSkuFeature(feature_obj);
                feature.setValue(feature.getButtonValue(button));
                feature.getElement().change();
                self.setSkuByFeature(feature_obj);
            },1);

        });
        setTimeout(function(){
            self.getForm().find(sel.sku_feature+":first").change();
        },50);

    }
};


