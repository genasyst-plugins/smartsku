<?php

class shopSmartskuPluginBackendSettingsController extends waViewController {
    
    public function execute() {
        if(waRequest::method()=='get') {
         $action = new shopSmartskuPluginBackendSettingsAction();
            echo $action->display();
        } else {
           $action = new shopSmartskuPluginBackendSettingsSaveController();
          $action->run();
        }
    }
}
