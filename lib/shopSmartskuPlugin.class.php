<?php
class shopSmartskuPlugin extends shopPlugin
{
    const APP = 'shop';
    const PLUGIN_ID = 'smartsku';
    const CONFIG_FILE = 'config.php';
    const GENERAL_STOREFRONT = 'general';

    protected static $config = null;

    protected static $action = '';

    public static function getAppConfig()
    {
        return  wa('shop')->getConfig();
    }
    public static function getPluginSettings($storefront = null) {
       if(self::$config == null) {
          $settings = new shopSmartskuPluginSettings($storefront);
          if ((isset($settings['debug']) && $settings['debug']==1 && shopSmartskuPlugin::isDebugRights()) && wa()->getEnv() == 'frontend') {
               self::$config = new shopSmartskuPluginDebugSettings($storefront);
           } else {
               self::$config = $settings;
           }
       }
        return self::$config;
    }
    protected static function setAction($action) {
        self::$action = $action;
    }
    public static function init($product){
        return  shopSmartskuPluginView::init($product);
    }
    public function frontendProduct($data) {
        shopSmartskuPluginView::getProductData($data, true);
        if(!shopSmartskuPlugin::isAction() || !$data['smartsku']->isAction()) {
            return;
        }
        $settings = self::getPluginSettings();
        $features_settings = $settings->getFeaturesSettings();
        $sku_features = wa()->getView()->getVars('features_selectable');
        if(is_array($sku_features) && !empty($sku_features)) {
            foreach ($sku_features as &$v) {
                if(isset($features_settings[$v['id']])) {
                    $feature_settings = $features_settings[$v['id']];
                    $v['view_type'] = $features_settings->getDefault('view_type');
                    if(isset($feature_settings['view_type'])) {
                        $v['view_type'] = $feature_settings['view_type'];
                    }
                    $v['view_name'] = $v['name'];
                    if(isset($feature_settings['view_name']) && !empty($feature_settings['view_name'])) {
                        $v['name'] = $feature_settings['view_name'];
                    }
                    $v['view_name_hide'] = $features_settings->getDefault('view_name_hide');
                    if(isset($feature_settings['view_name_hide']) && !empty($feature_settings['view_name_hide'])) {
                        $v['name']  = '';
                    }
                }
            }
            unset($v);
            wa()->getView()->assign('features_selectable', $sku_features);
        }
    }
    public function frontendProducts(&$data) {
        if(!shopSmartskuPlugin::isAction()) {
            return;
        }
        if(isset($data['products']) && !empty($data['products'])) {
            foreach ($data['products'] as $id => &$product) {
                shopSmartskuPluginView::getProductData($product);
            }
            unset($product);
        }
    }
    
    public function frontendHead() {
        $debug = waRequest::get('smartsku_plugin_debug');
        if(!is_null($debug)) {
            wa()->getResponse()->setCookie('smartsku_plugin_debug', $debug); //для удаленной отладки
        }
        if(self::isAction()) {
            return shopSmartskuPluginView::getHead();
        }
        return '';
    }
    public static function isDebugRights() {
        if(wa()->getUser()->isAdmin(shopSmartskuPlugin::APP) || waRequest::cookie('smartsku_plugin_debug')=='1') {
            return true;
        }
        return false;
    }
    public static function isAction() {
        $settings = self::getPluginSettings();
        if(waRequest::isMobile() && $settings['status_mobile']=='1' && (!self::isDebug() || shopSmartskuPlugin::isDebugRights())) {
            return true;
        } elseif($settings['status']=='1' && (!self::isDebug() || shopSmartskuPlugin::isDebugRights())) {
            return true;
        }
        return false;
    }
   public static function isDebug() {
        $settings = self::getPluginSettings();

        if(isset($settings['debug']) && $settings['debug']==1) {
            return true;
        }
       return false;
    }
    /**
     * Возвращает URL плагина от корня домена
     * @param bool $absolute
     * @return string
     */
    public static function getUrlStatic($absolute = false) {
        return wa()->getAppStaticUrl(self::APP, $absolute).'plugins/'.self::PLUGIN_ID.'/';
    }
}
