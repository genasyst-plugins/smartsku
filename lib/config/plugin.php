<?php
return array(
    'name'     => 'Умные артикулы',
    'version'  => '1.3.5',
    'critical' => '1.3.4',
    'img'=>'img/smartsku.png',
    'vendor'   => '990614',
    'shop_settings' => true,
    'frontend' => true,
    'custom_settings' => true,
    'handlers' =>  array(
        'frontend_head' => 'frontendHead',
        'frontend_product' => 'frontendProduct',
        'frontend_products' => 'frontendProducts'
    ),
);
