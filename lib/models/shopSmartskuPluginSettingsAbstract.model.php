<?php
abstract class shopSmartskuPluginSettingsAbstractModel extends waModel
{
    abstract public function getByStorefront($storefront_id, $data_id = null);
    abstract public function saveByStorefront($storefront_id, $data_id, $values = null);
}

