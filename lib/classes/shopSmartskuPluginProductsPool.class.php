<?php
class shopSmartskuPluginProductsPool {

    protected static $products = array();
    protected static $uids = array();
    public static function getUid() {
        $uid = rand(11111, 99999);
        if(!isset(self::$uids[$uid])) {
            self::$uids[$uid] = true;
            return $uid;
        } else {
            return self::getUid();
        }
    }
    /* Не плодим объекты shopProduct
    *  А то мало ли у клиента хостинг с квотой диска 2 ГБ, да и скорее всего памятью 16 МБ!
    */
    public static function getProduct($data = array()) {
        if(isset($data['id'])) {
            if(!isset(self::$products[$data['id']])) {
                self::$products[$data['id']] = new shopSmartskuPluginProductDecorator($data, true);
            }
            return  self::$products[$data['id']];
        }
        return null;
    }
}