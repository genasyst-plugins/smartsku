<?php
abstract class shopSmartskuPluginSettingsAbstract implements ArrayAccess {
    protected $settings = null;
    protected $model = null;
    protected $data = array();
    // protected $model_class_name = '';
    // protected $default_settings =  array();
    public function __construct($settings = null)  {
        $this->settings = $settings;
        $this->setSettings();
        $this->init();
    }
    protected function setSettings() {
        $data = $this->getModel()->getByStorefront((string)$this->getStorefront()->getId());
        if(!empty($data)) {
            $this->data = $data;
        }
    }
    protected function init(){}
    public function getDefault($k = null) {
        if(isset($this->default_settings[$k])) {
            return $this->default_settings[$k];
        } elseif ($k==null) {
            return $this->default_settings;
        }
        return null;
    }
    public function getModel() {
        if($this->model == null) {
            $class_name = $this->model_class_name;
            $this->model = new $class_name();
        }
        return $this->model;
    }
    public function getStorefront() {
        return $this->settings->getStorefront();
    }

    abstract public function save($data);

    public function offsetExists($offset)
    {
        if(array_key_exists($offset,$this->data)) {
            return true;
        }
        return false;
    }

    public function offsetGet($offset)
    {

        if($this->offsetExists($offset)) {
           return  $this->data[$offset];
        }
        return null;
    }

    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

}