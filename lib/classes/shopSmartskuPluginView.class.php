<?php
class shopSmartskuPluginView {

    protected static $theme = null;
    protected static $templates = null;

    public static function getTemplates() {
        if(self::$templates==null) {
            self::$templates = new shopSmartskuPluginTemplates(shopSmartskuPlugin::getPluginSettings());
        }
        return self::$templates;
    }
    public static function init(&$_product) {
        $product_data = array();
        self::getProductData($_product);
        $product = $_product[self::getKey()];
        if(!shopSmartskuPlugin::isAction() || !$product->isAction()) {
            return '<span id="smartsku-id-'.$product->getUid().'" class="smartsku-id"></span>';
        }

        /// Данные продукта основные
        $product_data['uid'] = $product->getUid();
        $product_data['currency'] = $product['currency_info'];
        $skus = $product['skus'];
        $sku_type = $product['sku_type'];
        if(count($skus)>1 or $sku_type ) {
            $product_data['services']  = $product['sku_services'];
        } else {
            // Если один артикул, надо передать его цену, иначе ломается подсчет из-за отсутствия в теге атрибута data-price
            $sku = $skus[$product['sku_id']];
            $product_data['sku']  = $sku;
        }
        if($sku_type) {
            $product_data['features']  = $product['sku_features_selectable'];
        } elseif (count($skus)>1) {
            $product_data['skus'] = $skus;
        }
        $product_data['type_id'] =  $product['type_id'];

        /// Данные из настроек
        $settings = shopSmartskuPlugin::getPluginSettings();
        $product_data['smartsku'] = $settings->getSmartSkuSettings()->getSettings();
        $product_data['debug'] = $settings['debug'];
        $product_data['hide_stocks']  = $product->isAction('stocks')?'0':'1';
        $product_data['hide_services']  = $product->isAction('services')?'0':'1';
        $product_data['smartsku']['smart_sku_class_grey'] = 'smartsku_plugin-feature-grey';
        $product_data['smartsku']['smart_sku_class_hide'] = 'smartsku_plugin-feature-hide';
        if(isset($settings['smart_sku_hide_style']) && !empty($settings['smart_sku_hide_style'])) {
            $product_data['smartsku']['smart_sku_class_grey'] = $settings['smart_sku_class_grey'];
            $product_data['smartsku']['smart_sku_class_hide'] = $settings['smart_sku_class_hide'];
        }

        $html ='';

        // Костыль для фиьтров некоторых тем дизайна, скрипты раньше печати выполнялись
        if(waRequest::isXMLHttpRequest()) {
            $html .=  '<span id="smartsku-id-'.$product->getUid().'" class="smartsku-id"></span><script type="text/javascript">(function($) {$(document).ready(function(){if(typeof smartskuPluginProduct == "function") {setTimeout(function(){var smartsku_plugin_product =  new smartskuPluginProduct("#smartsku-id-'.$product->getUid().'", '.json_encode($product_data).')}, 100);}})})(jQuery);</script>';
        } else {
            $html .=  '<span id="smartsku-id-'.$product->getUid().'" class="smartsku-id"></span><script type="text/javascript">(function($) {$(document).ready(function(){if(typeof smartskuPluginProduct == "function") {var smartsku_plugin_product =  new smartskuPluginProduct("#smartsku-id-'.$product->getUid().'", '.json_encode($product_data).')}})})(jQuery);</script>';
        }
        return $html;

    }
    public static function displayOptions(&$product) {
        self::getProductData($product);
        if(!shopSmartskuPlugin::isAction() || !$product['smartsku']->isAction()) {
            return '';
        }
        $html = self::fetch($product, self::getTemplates()->getTemplate('options'));
        return $html;
    }

    public static function getProductData(&$_product, $change_view = false) {
        if(!isset($_product[self::getKey()])) {
            $_product[self::getKey()] = new shopSmartskuPluginProduct($_product);
        }
        if($_product[self::getKey()]->isAction()) {
            $settings = shopSmartskuPlugin::getPluginSettings();
            $product = $_product['smartsku'];
            $skus = false;
            if(isset($product['skus'])) {
                $skus = $product['skus'];
            }
            if(is_array($skus) && count($skus)>1)  {
                $smartsku = $settings->getSmartSkuSettings()->getSettings();
                $delete_not_available = false;

                // Если включена замена на доступный и полное скрытие и это не выбор характеристик
                if(isset($smartsku['smart_sku_replace']) && $smartsku['smart_sku_replace']=='1'
                    && $settings['smart_sku_hide_not_available_type']=='2') {
                    $delete_not_available = true;
                }
                $available_sku = false;
                if(isset($product['skus'][$product['sku_id']]) && $product->isAvailableSku($product['sku_id'])) {
                    $available_sku = $product['sku_id'];
                } elseif(count($skus) == 1) {
                    $available_sku = $product['sku_id'];
                }
                $delete_skus = array();
                if(count($skus)>1) {
                    foreach($skus as $v) {
                        if(!$product->isAvailableSku($v)) {
                            if($delete_not_available && count($skus)>1) {
                                $id = $v['id'];
                                $delete_skus[$id] = $id;
                            }
                        } elseif(!$available_sku) {
                            $available_sku = $v['id'];
                        }
                    }
                }
                // Ошибка когда нет ни одного доступного
                if(count($delete_skus) == count($skus) && !$available_sku) {
                    $available_sku = reset($delete_skus);
                    unset($delete_skus[$available_sku]);
                }
                if($delete_not_available && count($skus)>1) {
                    if(!empty($delete_skus) && $change_view)  {
                        self::setFeaturesValues($_product, $delete_skus, $available_sku);
                    }
                }
                if((isset($smartsku['smart_sku_replace']) && $smartsku['smart_sku_replace']=='1' && $change_view) ||
                    (isset($settings['available_sku']) && $settings['available_sku']=='1'))  {
                    $_product['sku_id'] = $available_sku;
                    $_product['price'] = $skus[$available_sku]['price'];
                    $_product['compare_price'] = $skus[$available_sku]['compare_price'];
                    //
                    $_product[self::getKey()]['sku_id'] = $available_sku;
                    $_product[self::getKey()]['price'] = $skus[$available_sku]['price'];
                    $_product[self::getKey()]['compare_price'] = $skus[$available_sku]['compare_price'];
                }
                $_product[self::getKey()]['skus'] = $skus;
                $_product['skus'] = $skus;
            }
        }

    }

    protected function setFeaturesValues(&$product, $delete_skus, $available_sku) {
        $sku_features = wa()->getView()->getVars('features_selectable');
        if(is_array($sku_features) && !empty($sku_features) && count($sku_features)==1) {
            $product_features_model = new shopProductFeaturesModel();
            foreach ($product_features_model->getSkuFeatures($product->id) as $sku_id => $f) {
                if(array_key_exists($sku_id, $delete_skus))  {
                    foreach ($f as $feature_id => $value_id) {
                        unset($sku_features[$feature_id]['values'][$value_id]);
                    }
                } else if($sku_id == $available_sku) {
                    $dd = array();
                    foreach ($f as $feature_id => $value_id) {
                        $dd[$feature_id] = $value_id;
                    }
                    $product[self::getKey()]['sku_features'] = $dd;
                    $product['sku_features'] = $dd;
                }
            }
            wa()->getView()->assign('features_selectable',  $sku_features);
        }
    }
    protected static function getSettings() {
        return shopSmartskuPlugin::getPluginSettings();
    }
    protected static function getKey(){
        return shopSmartskuPlugin::PLUGIN_ID;
    }
    protected static function getView() {
        return wa()->getView();
    }
    protected static function fetch($product, $template) {
        $view = self::getView();
        $view->assign('product', $product['smartsku']);
        return $view->fetch($template);
    }
    public static function getHead() {
        $html = '';
        if(shopSmartskuPlugin::isAction()) {
            $html .= '<style>.smartsku_plugin-product ul.skus li {margin: 0;} .smartsku_plugin-product .smartsku-feature-value {margin-top:1em;padding-left:5px;} .smartsku_plugin-product .inline-select a.smartsku_plugin-feature-grey{color: #ccc !important; border-color: #efefef !important;} .smartsku_plugin-product .inline-select:not(.color) a.smartsku_plugin-feature-grey {background-color: #efefef !important;} .smartsku_plugin-product .inline-select a.smartsku_plugin-feature-grey:hover{color:#ccc !important;} .smartsku_plugin-product .inline-select a.smartsku_plugin-feature-grey:visited{color:#ccc !important;} .smartsku_plugin-product .inline-select.color a.smartsku_plugin-feature-grey{border-color: #ccc !important;opacity: 0.6;} .smartsku_plugin-product option.smartsku_plugin-feature-grey{color: #888 !important; border-color: #e8e8e8 !important; background-color: #e8e8e8;} .smartsku_plugin-product .inline-select a.smartsku_plugin-feature-hide {display: none !important;} .smartsku_plugin-product option.smartsku_plugin-feature-hide {display: none !important;} .smartsku_plugin-product .skus .smartsku_plugin-feature-grey { color: #d4d3d3 !important; text-decoration: line-through;} .smartsku_plugin-product .skus .smartsku_plugin-feature-grey .price, .smartsku_plugin-product .skus .smartsku_plugin-feature-grey span.price {font-weight: bold; color: #ccc !important; font-size: 100%;} </style>';
            $settings = shopSmartskuPlugin::getPluginSettings();
            if(isset($settings['style_default']) && $settings['style_default'] == 1) {
                $html .= '<link href="'.shopSmartskuPlugin::getUrlStatic().'css/smartskuDefaultFrontend.css'.($settings['debug']=='1'?'?_t='.time():'').'" rel="stylesheet" type="text/css">';
            }
            $app = wa('shop');
            $wa_url = $app->getAppUrl();
            $html .= '<script type="text/javascript">var shop_smartsku_wa_url = "'.$wa_url.'";</script>';
            if($settings['debug']=='1') {
                $debug_action = new shopSmartskuPluginDebugAction(array());
                $debug_html = urlencode($debug_action->display());
                $html .= '<script type="text/javascript">
                   $(document).ready(function() {
                       $("body").append(decodeURIComponent("'.$debug_html.'".replace(/\+/g, \' \')));
                   });
                   </script>';
                $html .= '<script type="text/javascript" src="'.shopSmartskuPlugin::getUrlStatic().'js/smartskuPluginProduct.js?_t='.time().'"></script>';
            } else {
                $html .= '<script type="text/javascript" src="'.shopSmartskuPlugin::getUrlStatic().'js/smartskuPluginProduct.min.js"></script>';
            }
            $templates = self::getTemplates();
            $html .= $templates->getThemeCorrection();

            $templates = self::getTemplates();
            $html .= $templates->getTemplate('js');
            $html .= $templates->getTemplate('css');

        }


        return $html;
    }

}