<?php
class shopSmartskuPluginSettings extends shopSmartskuPluginSettingsAbstract {
    protected $default_settings =  array(
        // global
        'status'         => 1,
        'status_mobile'  => 1,
        'hide_stocks'    => 0,
        'hide_services'  => 0,
        'debug'  => 0,

        // smart sku
        'available_sku' => 0,
        'smart_sku'                         => 1, // Общая настройка
        'smart_sku_replace'                 => 0, // Менять ли артикул на доступный
        'smart_sku_hide_single_feature'     => 0, // Скрывать характеристику если всего один вариант выбора
        'smart_sku_hide_multi_feature'      => 0, // Скрывать характеристику если всего один вариант выбора при наличии нескольких характеристик
        'smart_sku_hide_not_available_type' => 1, // Тип скрытия характеристик недоступного артикула
        'smart_sku_hide_non_existent_type'  => 1, // Тип скрытия характеристик несуществующего артикула

        'smart_sku_hide_style'              => 0,  // Свои классы для  скрытия
        'smart_sku_class_grey'              => '', // Класс частичного скрытия
        'smart_sku_class_hide'	            => '', // Класс полного скрытия

        // view
        'style_default'      => 1,
        'template_type'      => 'theme',
        'template_options'   => '',
        'template_js' => '',
        'template_css' => '',

    );

    /* Переменная не используется, она показывается структуру всех натсроек */
    protected $objects_default_settings = array(
        'feature_settings' => array(
            'view_type'      => 'select', // Вид показа выбираемой характеристики
            'view_name'      => '',       // Альтернативное название характеристики
            'view_name_hide' => 0         // Скрыть имя характеристики
        ),
        'product_type_settings' => array(
            'status_off'       => '0', // выключение плагина
            'status_mobile_off'=> '0', // выключение плагина для мобильников
            'hide_stocks'      => '0', // скрыть склады
            'hide_services'    => '0', // скрыть сервисы
        ),
        'smart_sku_settings' =>  array(
            'smart_sku'                         => 1, // Общая настройка
            'smart_sku_replace'                 => 0, // Менять ли артикул на доступный
            'smart_sku_hide_single_feature'     => 0, // Скрывать характеристику если всего один вариант выбора
            'smart_sku_hide_multi_feature'      => 0, // Скрывать характеристику если всего один вариант выбора при наличии нескольких характеристик
            'smart_sku_hide_not_available_type' => 1, // Тип скрытия характеристик недоступного артикула
            'smart_sku_hide_non_existent_type'  => 1, // Тип скрытия характеристик несуществующего артикула

            'smart_sku_hide_style'              => 0,  // Свои классы для  скрытия
            'smart_sku_class_grey'              => '', // Класс частичного скрытия
            'smart_sku_class_hide'	            => '', // Класс полного скрытия
        ),
    );
    protected $model_class_name = 'shopSmartskuPluginSettingsModel';
    protected $settings_objects = array(
        'feature_settings' => null,
        'product_type_settings' => null,
        'smart_sku_settings' => null
    );
    protected $storefront = null;
    public function __construct($storefront = null)  {
        $this->setStorefront($storefront);
        $this->setSettings();
        $this->init();
    }
    public function __get($name) {
        if($name=='data') {
            return array_merge($this->data, $this->settings_objects);
        }
        return $this->$name;
    }
    protected function init() {
        if(empty($this->data) &&  wa()->getEnv() == 'frontend') {
            $this->setStorefront(shopSmartskuPlugin::GENERAL_STOREFRONT);
            $this->setSettings();
        }
        $this->setSettingsObjects();
    }

    protected function setSettingsObjects() {
        $this->settings_objects['feature_settings'] = new shopSmartskuPluginFeaturesSettings($this);
        $this->settings_objects['product_type_settings'] = new shopSmartskuPluginProductTypesSettings($this);
        $this->settings_objects['smart_sku_settings'] = new shopSmartskuPluginSmartSkuSettings($this);
    }
    protected function getSettingsObject($name) {
        if(array_key_exists($name, $this->settings_objects)) {
            if($this->settings_objects[$name] == null) {
                $this->setSettingsObjects();
            }
            return $this->settings_objects[$name];
        }
        return null;
    }
    /* Настройки характеристик на витрине  */
    public function getFeaturesSettings() {
        return $this->getSettingsObject('feature_settings');
    }
    /* Настройки по типу продукта  */
    public function getProductTypeSettings() {
        return $this->getSettingsObject('product_type_settings');
    }
    public function getSmartSkuSettings() {
        return $this->getSettingsObject('smart_sku_settings');
    }
    public function getImageSizes() {
       return self::getAppConfig()->getImageSizes();
    }
    public static function getAppConfig()
    {
        return  shopSmartskuPlugin::getAppConfig();
    }
    /* Возвращает все поселения магазина */
    public static function getStorefronts()  {
        $routing = wa()->getRouting();
        $storefronts = array(shopSmartskuPlugin::GENERAL_STOREFRONT);
        $domains = $routing->getByApp('shop');
        // Пробегаем по доменам
        foreach ($domains as $domain => $domain_routes) {
            // Забираем все отдельные поселения
            foreach ($domain_routes as $route)  {
                $storefronts[] = $domain.'/'.$route['url'];
            }
        }
        return $storefronts;
    }
    /* Возвращает текущую витрину */
    protected function setStorefront($storefront = null) {
        if($storefront !== null) {
            $this->storefront = new shopSmartskuPluginStorefront($storefront);
        } elseif($this->storefront == null) {
            $routing = wa()->getRouting();
            $domain = $routing->getDomain();
            $route = $routing->getRoute();
            $storefronts = $this->getStorefronts();
            $currentRouteUrl = $domain.'/'.$route['url'];
            $storefront = in_array($currentRouteUrl, $storefronts) ? $currentRouteUrl : shopSmartskuPlugin::GENERAL_STOREFRONT;
            $this->storefront = new shopSmartskuPluginStorefront($storefront);
        }
    }
    public function getStorefront() {
        if($this->storefront === null) {
            $this->setStorefront();
        }
        return $this->storefront;
    }
    
    public function getTemplates() {
        return new shopSmartskuPluginTemplates($this);
    }
    public function save($data) {
        if(is_array($data)) {
            if(isset($data['settings'])) {
                $settings_model = new shopSmartskuPluginSettingsModel();
                foreach($data['settings'] as $k => $v) {
                    $s_data = array(
                        'key' => $k,
                        'value' => $v,
                        'storefront_id' => $this->getStorefront()->getId()
                    );
                    $settings_model->insert($s_data,1);
                }
            }
           unset($data['settings']);
            // Сохраняем другие данные
            if(!empty($data)) {
                foreach ($data as $k => $v) {
                    if(isset($this->settings_objects[$k]) && is_object($this->settings_objects[$k])) {
                        $this->settings_objects[$k]->save($v);
                    }
                }
            }
        }
    }
}