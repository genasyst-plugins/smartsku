<?php

class shopSmartskuPluginFrontendProductController extends waController {
    
    public function execute() {
       $id = waRequest::param('product_id',0,waRequest::TYPE_INT);
        if($id>0) {
            $product = new shopProduct($id);
            wa()->getResponse()->sendHeaders();
            echo shopSmartskuPluginView::init($product);
        }
    }
}
 