<?php
class shopSmartskuPluginProductDecorator implements ArrayAccess {

    protected $product = null;
    protected $methods = array(
        'currency_info' => 'getCurrencyInfo',
        'sku_services' => 'getServicesVars',
        'services' => 'getServicesVars',
        'skus' => 'getSkus',
        'sku_features_selectable' => 'getSkuFeatures',
        'sku_features' => 'getSkuFeatures',
        'features_selectable' => 'getFeaturesSelectable',
        'selectable_features_control' => 'getSelectableFeaturesControl',
        'stocks' => 'getStocks',
        'sku_id' => 'getSkuId'
    );
    protected $data = array(
        'currency_info' => null,
        'sku_services' => null,
        'services' => null,
        'skus' => null,
        'sku_features_selectable' => null,
        'sku_features' => null,
        'features_selectable' => null,
        'selectable_features_control' => null,
        'stocks' => null,
        'sku_id' => null,
        'skus_view_type' => null,
    );

    public function __construct($data = array(), $is_frontend = true)
    {
        if($data instanceof shopProduct) {
            $this->product = $data;
        } elseif(is_array($data) && !empty($data['id'])) {
            $this->product = new shopProduct($data, $is_frontend);
        }
    }
    /* Все данные проходят через этот метод */
    protected function getData($name) {
        if(array_key_exists($name, $this->data)) {
            if(is_null($this->data[$name])) {
                $method = $this->methods[$name];
                $this->$method();
            }
            return $this->data[$name];
        } elseif(isset($this->product[$name])) {
            return $this->product[$name];
        } else {
            return null;
        }
    }
    public function getSkuId() {
        $product = $this->getProduct();
        $this->setData('sku_id', $product['sku_id']);
        $settings = shopSmartskuPlugin::getPluginSettings();
        if(isset($settings['available_sku']) && $settings['available_sku']=='1' && $this->getData('status')) {
            $skus = $this->getData('skus');
            if(array_key_exists($product['sku_id'], $skus) && $this->isAvailableSku($skus[$product['sku_id']])) {
                return;
            }
            foreach ($skus as $_sku) {
                if($this->isAvailableSku($_sku)) {
                    $this->setData('sku_id', $_sku['id']);
                    break;
                }
            }
        }
    }
    public function isAvailableSku($sku_data) {
        $sku = false;
        if (is_array($sku_data) && array_key_exists('available', $sku_data)) {
            $sku = $sku_data;
        } elseif(!is_array($sku_data) && is_numeric($sku_data)) {
            $skus = $this->getData('skus');
            if(isset($skus[$sku_data])) {
                $sku = $skus[$sku_data];
            }
        }
        if($sku && $sku['available'] && ($this->getConfig()->getGeneralSettings('ignore_stock_count') || $sku['count'] === null || $sku['count'] > 0)) {
           return true;
        }
        return false;
    }
    /* 
    * Основные методы получения дополнительных данных продукта 
    */
    /* Склады */
    protected function getStocks() {
        if(method_exists('shopHelper','getStocks')) {
            $this->setData('stocks',shopHelper::getStocks(true));
        } else {
            // Для старой версии, проверял на 6.3
            $stock_model = new shopStockModel();
            $this->setData('stocks', $stock_model->getAll('id'));
        }
    }
    /* Информация о сервисах продукта */
    protected function getServicesVars() {
        $type_services_model = new shopTypeServicesModel();
        $services = $type_services_model->getServiceIds($this->getData('type_id'));

        // Fetch services
        $service_model = new shopServiceModel();
        $product_services_model = new shopProductServicesModel();
        $services = array_merge($services, $product_services_model->getServiceIds($this->getData('id')));
        $services = array_unique($services);
        $services = $service_model->getById($services);
        shopRounding::roundServices($services);

        // Convert service.price from default currency to service.currency
        foreach ($services as &$s) {
            $s['price'] = shop_currency($s['price'], null, $s['currency'], false);
        }
        unset($s);

        // Fetch service variants
        $variants_model = new shopServiceVariantsModel();
        $rows = $variants_model->getByField('service_id', array_keys($services), true);
        shopRounding::roundServiceVariants($rows, $services);
        foreach ($rows as $row) {
            if (!$row['price']) {
                $row['price'] = $services[$row['service_id']]['price'];
            } elseif ($services[$row['service_id']]['variant_id'] == $row['id']) {
                $services[$row['service_id']]['price'] = $row['price'];
            }
            $services[$row['service_id']]['variants'][$row['id']] = $row;
        }

        // Fetch service prices for specific products and skus
        $rows = $product_services_model->getByField('product_id', $this->getData('id'), true);
        shopRounding::roundServiceVariants($rows, $services);
        $skus_services = array(); // sku_id => [service_id => price]
        $frontend_currency = wa('shop')->getConfig()->getCurrency(false);
        $skus = $this->getData('skus');
        foreach ($skus as $sku) {
            $skus_services[$sku['id']] = array();
        }
        foreach ($rows as $row) {
            if (!$row['sku_id']) {
                if (!$row['status']) {
                    // remove disabled services and variants
                    unset($services[$row['service_id']]['variants'][$row['service_variant_id']]);
                } elseif ($row['price'] !== null) {
                    // update price for service variant, when it is specified for this product
                    $services[$row['service_id']]['variants'][$row['service_variant_id']]['price'] = $row['price'];
                    // !!! also set other keys related to price
                }
                if ($row['status'] == shopProductServicesModel::STATUS_DEFAULT) {
                    // default variant is different for this product
                    $services[$row['service_id']]['variant_id'] = $row['service_variant_id'];
                }
            } else {
                if (!$row['status']) {
                    $skus_services[$row['sku_id']][$row['service_id']][$row['service_variant_id']] = false;
                } else {
                    $skus_services[$row['sku_id']][$row['service_id']][$row['service_variant_id']] = $row['price'];
                }
            }
        }

        // Fill in gaps in $skus_services
        foreach ($skus_services as $sku_id => &$sku_services) {
            $sku_price = $skus[$sku_id]['price'];
            foreach ($services as $service_id => $service) {
                if (isset($sku_services[$service_id])) {
                    if ($sku_services[$service_id]) {
                        foreach ($service['variants'] as $v) {
                            if (!isset($sku_services[$service_id][$v['id']]) || $sku_services[$service_id][$v['id']] === null) {
                                $sku_services[$service_id][$v['id']] = array($v['name'], $this->getPrice($v['price'], $service['currency'], $sku_price, $this->getData('currency')));
                            } elseif ($sku_services[$service_id][$v['id']]) {
                                $sku_services[$service_id][$v['id']] = array($v['name'], $this->getPrice($sku_services[$service_id][$v['id']], $service['currency'], $sku_price, $this->getData('currency')));
                            }
                        }
                    }
                } else {
                    foreach ($service['variants'] as $v) {
                        $sku_services[$service_id][$v['id']] = array($v['name'], $this->getPrice($v['price'], $service['currency'], $sku_price, $this->getData('currency')));
                    }
                }
            }
        }
        unset($sku_services);

        // disable service if all variants are disabled
        foreach ($skus_services as $sku_id => $sku_services) {
            foreach ($sku_services as $service_id => $service) {
                if (is_array($service)) {
                    $disabled = true;
                    foreach ($service as $v) {
                        if ($v !== false) {
                            $disabled = false;
                            break;
                        }
                    }
                    if ($disabled) {
                        $skus_services[$sku_id][$service_id] = false;
                    }
                }
            }
        }

        // Calculate prices for %-based services,
        // and disable variants selector when there's only one value available.
        foreach ($services as $s_id => &$s) {
            if (!$s['variants']) {
                unset($services[$s_id]);
                continue;
            }
            if ($s['currency'] == '%') {
                foreach ($s['variants'] as $v_id => $v) {
                    $s['variants'][$v_id]['price'] = $v['price'] * $skus[$this->getData('sku_id')]['price'] / 100;
                }
                $s['currency'] = $this->getData('currency');
            }

            if (count($s['variants']) == 1) {
                $v = reset($s['variants']);
                if ($v['name']) {
                    $s['name'] .= ' '.$v['name'];
                }
                $s['variant_id'] = $v['id'];
                $s['price'] = $v['price'];
                unset($s['variants']);
                foreach ($skus_services as $sku_id => $sku_services) {
                    if (isset($sku_services[$s_id]) && isset($sku_services[$s_id][$v['id']])) {
                        $skus_services[$sku_id][$s_id] = $sku_services[$s_id][$v['id']][1];
                    }
                }
            }
        }
        unset($s);

        uasort($services, array('shopServiceModel', 'sortServices'));

        $this->setData('sku_services', $skus_services);
        $this->setData('services', $services);
    }
    /* Информация о характеристиках артикулов */
    protected function getSkuFeatures() {
        if ($this->getData('sku_type') == shopProductModel::SKU_TYPE_SELECTABLE) {

            $features_selectable = $this->getData('features_selectable');
            $product_features_model = new shopProductFeaturesModel();
            $sku_features = $product_features_model->getSkuFeatures($this->getData('id'));

            $sku_selectable = array();
            $skus = $this->getData('skus');
            $currency = $this->getData('currency');

            foreach ($sku_features as $sku_id => $sf) {
                if (!isset($skus[$sku_id])) {
                    continue;
                }
                $sku_f = "";

                foreach ($features_selectable as $f_id => $f) {
                    if (isset($sf[$f_id])) {
                        $sku_f .= $f_id.":".$sf[$f_id].";";
                    }
                }
                $sku = $skus[$sku_id];
                $sku['image_id'] = (int)$sku['image_id'];
                $sku_selectable[$sku_f] = array(
                    'id'        => $sku_id,
                    'price'     => (float)shop_currency($sku['price'], $currency, null, false),
                    'available' => $sku['available'],
                    'image_id'  =>  $sku['image_id'],
                    'sku_name'  =>  $sku['name'],
                    'sku_code'  =>  $sku['sku']
                );
                // Дополняем данные адресом картинки артикула
                if(isset($sku['image']) && !empty($sku['image'])) {
                    $sku_selectable[$sku_f]['image'] = $sku['image'];
                }
                if ($sku['compare_price']) {
                    $sku_selectable[$sku_f]['compare_price'] = (float)shop_currency($sku['compare_price'], $currency, null, false);
                }
            }
            $this->setData('sku_features_selectable',$sku_selectable);
            $this->setData('sku_features', ifset($sku_features[$this->getData('sku_id')], array()));
        }
    }
    /* Данные выбираемых характеристик */
    protected function getFeaturesSelectable() {
        $features_settings   = shopSmartskuPlugin::getPluginSettings()->getFeaturesSettings();
        $features_selectable = $this->product->features_selectable;
        if(is_array($features_selectable) && !empty($features_selectable)) {
            foreach ($features_selectable as &$v) {
                // Потом сделаю array_merge, пока так
                $feature_settings = $features_settings[$v['id']];
                $v['view_type'] = $features_settings->getDefault('view_type');
                if(isset($feature_settings['view_type'])) {
                    $v['view_type'] = $feature_settings['view_type'];
                }
                $v['view_name'] = $v['name'];
                if(isset($feature_settings['view_name']) && !empty($feature_settings['view_name'])) {
                    $v['view_name'] = $feature_settings['view_name'];
                }
                $v['view_name_hide'] = $features_settings->getDefault('view_name_hide');
                if(isset($feature_settings['view_name_hide'])) {
                    $v['view_name_hide'] = $feature_settings['view_name_hide'];
                    if(!empty($v['view_name_hide'])) {
                        $v['name'] = '';
                    }
                }
            }
            unset($v);
        }
        $this->setData('features_selectable', $features_selectable);
        return $features_selectable;
    }
    /* Данные артикулов не по характеристикам */
    protected function getSkus() {
        $skus = $this->product['skus'];
        $return_skus = array();
        if(is_array($skus) && !empty($skus)) {
            foreach ($skus as &$sku) {
                // Дополнительные данные доступности
                $sku['available'] = $this->isAvailableSku($sku);
                $return_skus[$sku['id']] = $sku;
            }
            unset($sku);
        }
        $this->setData('skus', $return_skus);
    }
    /* Объект продукта */
    private function getProduct() {
        return $this->product;
    }
    public function isAction($action = '') {
        $settings = shopSmartskuPlugin::getPluginSettings();
        $product_types_settings = $settings->getProductTypeSettings();
        $return = false;
        if(waRequest::isMobile()) {
            if(!isset($product_types_settings[$this->getData('type_id')]['status_mobile_off']) || empty($product_types_settings[$this->getData('type_id')]['status_mobile_off'])) {
                $return = true;
            }
        } elseif(!isset($product_types_settings[$this->getData('type_id')]['status_off']) || empty($product_types_settings[$this->getData('type_id')]['status_off'])) {
            $return =  true;
        }
        if($return && !empty($action)) {
            $action_setting = array(
                'stocks' => 'hide_stocks',
                'services' => 'hide_services'
            );
            if(array_key_exists($action,$action_setting)) {
                if(!empty($product_types_settings[$this->getData('type_id')][$action_setting[$action]]) ||
                    (isset($settings[$action_setting[$action]]) && !empty($settings[$action_setting[$action]]))) {
                    $return = false;
                }
            }
        }
        return $return;
    }

    /* 
    * Дополнительные методы для получения данных продукта
    */
    protected function getConfig() {
        return shopSmartskuPluginSettings::getAppConfig();
    }
    protected function getCurrencyInfo()
    {
        $currency = waCurrency::getInfo($this->getConfig()->getCurrency(false));
        $locale = waLocale::getInfo(wa()->getLocale());
        $this->setData('currency_info',array(
            'code'          => $currency['code'],
            'sign'          => $currency['sign'],
            'sign_html'     => !empty($currency['sign_html']) ? $currency['sign_html'] : $currency['sign'],
            'sign_position' => isset($currency['sign_position']) ? $currency['sign_position'] : 1,
            'sign_delim'    => isset($currency['sign_delim']) ? $currency['sign_delim'] : ' ',
            'decimal_point' => $locale['decimal_point'],
            'frac_digits'   => $locale['frac_digits'],
            'thousands_sep' => $locale['thousands_sep'],
        ));
    }
    protected function getPrice($price, $currency, $product_price, $product_currency) {
        if ($currency == '%') {
            return shop_currency($price * $product_price / 100, $product_currency, null, 0);
        } else {
            return shop_currency($price, $currency, null, 0);
        }
    }
    /* Доступ как к массиву */
    public function offsetExists($offset) {
        return (isset($this->data[$offset]) || isset($this->product[$offset]));
    }
    public function offsetGet($offset) {
        return $this->getData($offset);
    }
    public function offsetSet($offset, $value) {
        $this->setData($offset, $value);
    }
    public function offsetUnset($offset) {
        return true; // не надо ничего удалять!
    }

    public function __get($name) {
        return $this->getData($name);
    }
    public function __set($name, $value) {
       // Спасибо, не надо ничего записывать
    }

    public function __call($name, $args) {
        if (method_exists($this->product, $name)) {
            return call_user_func_array(array($this->product, $name), $args);
        }
        return null;

    }

    protected function setData($name, $value) {
        if(array_key_exists($name, $this->data)) {
            $this->data[$name] = $value;
        }
    }
}