<?php

class shopSmartskuPluginBackendSettingsSaveController extends waJsonController {
    
    public function execute() {
        $storefront = waRequest::get('storefront');
        if(empty($storefront)) {
            $storefront = shopSmartskuPlugin::GENERAL_STOREFRONT;
        }
        $settings = shopSmartskuPlugin::getPluginSettings($storefront);
        if(waRequest::method()=='get') {
          $this->errors[] = 'Неправильный запрос!';
        } else {
            $data = waRequest::post();
            $settings->save($data);
            $this->response = 'ok';
        }

        
    }
}
